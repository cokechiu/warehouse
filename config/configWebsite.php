<?php
	// Configure web and admin path, pull website default setting
	if (session_status() == PHP_SESSION_NONE) @session_start();
	
	// Get document root path
	$systemRootPath = $_SERVER["DOCUMENT_ROOT"];
	$systemRootPathArray = str_split($systemRootPath);
	$systemRootPath .= ( $systemRootPathArray[count($systemRootPathArray)-1] == "/" ) ? "" : "/";

	define("relativeWeb","");
	define("relativeAdmin","");
	define("rootPathWeb",$systemRootPath);
	define("rootPathAdmin",$systemRootPath.relativeAdmin);

	include( rootPathAdmin."libs/datatables/lib/DataTables.php" );

?>