<?php

include_once "../../config/configWebsite.php";
include_once "../../libs/datatables/media/php/DataTables.php";
include_once "../../phpclass/util.php";
include_once "pdfClientTemplate.php";

$util = new util();
$util -> setDebug(false);
isDirectLink();
foreach( $_REQUEST as $key => $value ) {
	$list[] = $key;
	$$key = $value;
}

// If pass single clientId, generate only one file , 
// otherewise generate all file and zip
// type single , multi , all
$type = "all";
if (isset($id)) {
	$type = "single";
}
if (isset($ids)) {
	$type = "multi";
}

switch($type) {
	case "single":
		$sql =  "
		select
			c.clientId
		from client
			c
		left join entry1 e1 on
			c.clientId = e1.clientId
		where
			c.activate = '1' AND c.closed = '1' AND c.clientId = '".addslashes($id)."'";
		break;
	case "multi":
		$clientIds = json_decode($ids);
		$inId = implode(",",$clientIds);

		$sql =  "
			select
				c.clientId 
			from client
				c
			left join entry1 e1 on
				c.clientId = e1.clientId
			where
				c.activate = '1' AND c.closed = '1' AND c.clientId in (".$inId.")";
		break;
	case "all":
		echo "請稍等...正在生成PDF檔案";
		$sql =  "select
				c.clientId
			from client
				c
			left join entry1 e1 on
				c.clientId = e1.clientId
			where
				c.activate = '1' AND c.closed = '1'";
		break;
}
if ($db->sql($sql)-> count() == 0) die();
$clientIds = $db->sql($sql)->fetchAll();

foreach($clientIds as $clientIdKey => $clientIdArray) {
	$clientId =  $clientIdArray["clientId"];
	//Select except password
	$sql =  "select
				c.clientId, c.profile, c.email, c.title, c.firstName, c.lastName, c.fullName, c.birthday, 
				c.contactAreaCode, c.contact, c.otherContactAreaCode, c.otherContact, c.address1, c.address2,
				c.country, c.language, c.date, c.reset, c.activateCode, c.activate, c.closed,
				e1.*,
				cty.*
			from client
				c
			left join entry1 e1 on
				c.clientId = e1.clientId
			left join country cty on
				c.country = cty.countryId
			where
				c.activate = '1' AND c.closed = '1' AND c.clientId = '".addslashes($clientId)."'";

	$client = $db->sql($sql)->fetch();

	$sql =  "select * from clientPaint where clientId = '".addslashes($clientId)."'";
	$clientPaints = $db->sql($sql)->fetchAll();
	$outputPath = adminAbsolutePath."upload/pdf/client/".$client["clientId"]." - ".$client["fullName"].".pdf";
	if (!file_exists($outputPath)) {
		generatePdf($client, $clientPaints, $outputPath);
	}
}

// Single type return file , other type return redirect link for zip
if ($type=="single") {
	$sql =  "
		select
			c.clientId, c.fullName
		from client
			c
		where
			c.activate = '1' AND c.closed = '1' AND c.clientId = '".addslashes($id)."'";

	$client = $db->sql($sql)->fetch();
	$rediectLink = "";
	$filePath = adminAbsolutePath."upload/pdf/client/".$id." - ".$client["fullName"].".pdf";

	outputStream($filePath, $id." - ".$client["fullName"].".pdf","application/pdf", false);

} else if ($type == "multi") {

	$zipFilePath = "../../upload/pdf/".time().".zip";

	$zip = new ZipArchive();
	$result_code = $zip->open($zipFilePath);

	if ($zip->open($zipFilePath, ZipArchive::CREATE)!==TRUE) {
		exit("cannot open <".$zipFilePath."<br>");
	}

	foreach($clientIds as $clientIdKey => $clientIdArray) {

		$clientId =  $clientIdArray["clientId"];
		$sql =  "select
					c.clientId, c.profile, c.email, c.title, c.firstName, c.lastName, c.fullName, c.birthday, 
					c.contactAreaCode, c.contact, c.otherContactAreaCode, c.otherContact, c.address1, c.address2,
					c.country, c.language, c.date, c.reset, c.activateCode, c.activate, c.closed,
					e1.*,
					cty.*
				from client
					c
				left join entry1 e1 on
					c.clientId = e1.clientId
				left join country cty on
					c.country = cty.countryId
				where
					c.activate = '1' AND c.closed = '1' AND c.clientId = '".addslashes($clientId)."'";
		$client = $db->sql($sql)->fetch();
		$zip->addFile(adminAbsolutePath."upload/pdf/client/".$client["clientId"]." - ".$client["fullName"].".pdf",
					 $client["clientId"]." - ".$client["fullName"].".pdf");
	}
	$zip->close();
	outputStream($zipFilePath,"client.zip","application/zip", true);


} else if ($type == "all") {
	$rediectLink = "../../upload/pdf/zip.php?folder=client";
}



function resize($img, $container) {
	$resize = [];
	$wRatio = $container["width"]/$img["width"];
	$hRatio = $container["height"]/$img["height"];
	//Scale fit to height
	if ($wRatio > $hRatio) {
		$resize["height"] = $container["height"];
		$resize["width"] = floor($hRatio * $img["width"]);
	} else {
		$resize["width"] = $container["width"];
		$resize["height"] = floor($wRatio * $img["height"]);
	}
	return $resize;
}

function outputStream($filePath, $fileName, $contentType, $unlink) {
	if (file_exists($filePath)) {
			
		header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
	
		// HTTP/1.1
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);

		// HTTP/1.0
		header("Pragma: no-cache");
		header('Expires: 0');
		header("Content-Type: application/pdf". $contentType);
		header("Content-Length:".filesize($filePath));
		header("Content-Disposition: attachment; filename=".$fileName);
		// fpassthru($fp);
		
		// readfile($attachment_location);
		// header($_SERVER["SERVER_PROTOCOL"] . " 200 OK");
		// header("Cache-Control: public"); // needed for internet explorer
		// header("Content-Type: application/zip");
		// header("Content-Transfer-Encoding: Binary");
		// header("Content-Length:".filesize($attachment_location));
		// header("Content-Disposition: attachment; filename=file.zip");
		// sendHeaders($photoPath, 'image/jpeg', $fileName);
		// readfile($attachment_location);
		set_time_limit(0);
		$file = @fopen($filePath,"rb");
		while(!feof($file))
		{
			print(@fread($file, 1024*8));
			ob_flush();
			flush();
		}
		if ($unlink) {
			unlink($filePath);
		}
	}
}

?>


<script>
	location.href = "<?php echo $rediectLink; ?>"
</script>
