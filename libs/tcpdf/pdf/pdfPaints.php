<?php

include "../../libs/datatables/media/php/DataTables.php";
include "../../phpclass/util.php";

$util = new util();
$util -> setDebug(false);
isDirectLink();

// foreach($_REQUEST as $key => $value) {
// 	$$key = $value;
// }
$sql =  "
		select
			*
		from 
			clientPaint cp
		where
			cp.clientId in (
				select
					c.clientId
				from client
					c
				left join entry1 e1 on
					c.clientId = e1.clientId
				where
					c.activate = '1' AND c.closed = '1'
			) and 
			cp.paint <> ''";
$clientPaints = $db->sql($sql)->fetchAll();
foreach($clientPaints as $key => $clientPaint) {

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

ini_set('memory_limit', '256M');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$HEADER_TITLE = "　參賽作品";

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));
// set default footer data
$pdf->setFooterData(array(0,64,0), array(0,0,0));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);
$fontname = TCPDF_FONTS::addTTFfont('../fonts/wqy_microhei.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont($fontname, '', 10);
$pdf->setHeaderFont(Array($fontname, '', 10));

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();
// if (file_exists("/home/digenet/public_html/client/wanfung/".$client['profile']) ) {
if ($clientPaint['paint']!=""){
	if (file_exists("../../../".$clientPaint['paint'])) {
		$html = <<<EOT
			<img src="../../../{$clientPaint['paint']}" width="700"></img>
EOT;
		$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
	}
}
// print a block of text using Write()
// $pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.

$pdf->Output("/home/digenet/public_html/client/wanfung/admin/upload/pdf/paint/".$clientPaint["clientPaintId"]." - ".$clientPaint["title"].".pdf", 'F');
// $pdf->Output($clientPaint["clientPaintId"]." - ".$clientPaint["title"].".pdf", 'I');
//============================================================+
// END OF FILE
//============================================================+
}

?>

<script>
	location.href = "../../upload/pdf/zip.php?folder=paint";
</script>
