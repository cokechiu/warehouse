<?php
    include "config/configWebsite.php";
    include "phplibs/class/utils.php";
    include "phplibs/class/systemCode.php";
    include "phplibs/class/dbHelper.php";
    include "phplibs/class/systemClass.php";

    $utils = new Utils();

    $system = new System();
    $system->db = $db;

    // Lingual seeting
    $lang = ( isset($_SESSION["wh"]["lang"]) ) ? $_SESSION["wh"]["lang"] : "en";
    $_SESSION["wh"]["lang"] = $lang;
    
?>