<?php

    /**
     *  Define system code constant here
     */

    /**
     *  System 100 - 199
     */
    define( "SUCCESS", 100 );
    define( "INVALID_REQUEST_METHOD", 101 );
    define( "MISSING_LANG", 102 );
    define( "MISSING_TOKEN", 103 );
    define( "INVALID_TOKEN", 104 );
    define( "FAILED_ADD_RECORD", 191 );
    define( "FAILED_UPDATE_RECORD", 192 );
    define( "FAILED_DELETE_RECORD", 193 );
    define( "NO_PERMISSION", 194 );
    define( "MISSING_SYSTEM_LANG_ID", 195 );

    /**
     *  Member 200-299
     */

    // Common 200-209
    define( "MISSING_EMAIL", 200 );
    define( "MISSING_PASSWORD", 201 );
    define( "MISSING_USER_ID", 202 );

    // Login 210-219
    define( "LOGIN_FAIL", 210 );
    define( "USER_DISABLED", 211 );
    define( "USER_DOESNT_EXIST", 212 );
    define( "INCORRECT_PASSWORD", 213 );
    define( "USER_LOGIN_ALREADY", 214 );

    // Create 220-229
    define( "MISSING_USERNAME", 220 );
    define( "DUPLICATED_EMAIL", 221 );
    define( "DUPLICATED_USERNAME", 222 );

    /**
     *  Products 300-399
     */

    // Common 300-309

    // Products - create & modify Brand 310-319
    define( "MISSING_BRANDNAME", 310 );
    define( "INCORRECT_BRANDNAME", 311 );
    define( "DUPLICATED_BRANDNAME", 312 );
    define( "MISSING_BRANDID", 313 );
    define( "INCORRECT_BRANDID", 314 );
    
    // Products - create & modify Category 320-329
    define( "MISSING_CATEGORYNAME", 320 );
    define( "INCORRECT_CATEGORYNAME", 321 );
    define( "DUPLICATED_CATEGORYNAME", 322 );
    define( "MISSING_CATEGORYID", 323 );
    define( "INCORRECT_CATEGORYID", 324 );
    define( "MISSING_PARENTID", 325 );

    // Products - create & modify Unit 330-339
    define( "MISSING_UNITNAME", 330 );
    define( "INCORRECT_UNITNAME", 331 );
    define( "DUPLICATED_UNITNAME", 332 );
    define( "MISSING_UNITID", 333 );
    define( "INCORRECT_UNITID", 334 );

    // Products - create & modify Supplier 340-349
    define( "MISSING_SUPPLIERNAME", 340 );
    define( "INCORRECT_SUPPLIERNAME", 341 );
    define( "DUPLICATED_SUPPLIERNAME", 342 );
    define( "MISSING_SUPPLIERID", 343 );
    define( "INCORRECT_SUPPLIERID", 344 );

    // Products - create & modify Supplier Contact 350-359
    define( "MISSING_SUPPLIERCONTACTNAME", 350 );
    define( "INCORRECT_SUPPLIERCONTACTNAME", 351 );
    define( "MISSING_SUPPLIERCONTACTEMAIL", 352 );
    define( "INCORRECT_SUPPLIERCONTACTEMAIL", 353 );
    define( "DUPLICATED_SUPPLIERCONTACTEMAIL", 354 );
    define( "MISSING_SUPPLIERCONTACTID", 355 );
    define( "INCORRECT_SUPPLIERCONTACTID", 356 );
    
    // Products - create & modify Product 360-369
    define( "MISSING_PRODUCTNAME", 360 );
    define( "INCORRECT_PRODUCTNAME", 361 );
    define( "DUPLICATED_PRODUCTNAME", 362 );
    define( "MISSING_PRODUCTCODE", 363 );
    define( "INCORRECT_PRODUCTCODE", 364 );
    define( "DUPLICATED_PRODUCTCODE", 365 );
    define( "MISSING_PRODUCTID", 366 );
    define( "INCORRECT_PRODUCTID", 367 );

    // Products - create & modify SKU Unit 370-379
    define( "MISSING_SKUUNITNAME", 370 );
    define( "INCORRECT_SKUUNITNAME", 371 );
    define( "DUPLICATED_SKUUNITNAME", 372 );
    define( "MISSING_SKUUNITID", 373 );
    define( "INCORRECT_SKUUNITID", 374 );

    // Products - create & modify SKU 380-389
    define( "MISSING_SKUCODE", 380 );
    define( "INCORRECT_SKUCODE", 381 );
    define( "DUPLICATED_SKUCODE", 382 );
    define( "MISSING_SKUID", 383 );
    define( "INCORRECT_SKUID", 384 );

    /**
     *  System menu 400-499
     */

    // Common 400-409

    // Create menu 410-419
    define( "MISSING_ICON", 410 );
    define( "MISSING_NAME", 411 );
    define( "MISSING_ROUTE", 412 );
    define( "INCORRECT_NAME_FORMAT", 413 );
    define( "MISSING_NAME_EN", 414 );
    define( "MISSING_NAME_TC", 415 );
    define( "MISSING_PARENT_MENU_ID", 416 );
    define( "MISSING_MENU_ID", 417 );
    define( "MISSING_API_URL", 418 );
    define( "MISSING_API_NAME", 419 );
    define( "MISSING_MENU_API_ID", 420 );

    /**
     *  Permission template 500-599
     */

    // Common 500-509

    // permission 510-599
    define( "MISSING_PERMISSION_TEMPLATE_NAME", 510 );
    define( "TEMPLATE_NAME_NOT_JSON", 511 );
    define( "MISSING_TEMPLATE_NAME_EN", 512 );
    define( "MISSING_TEMPLATE_NAME_TC", 513 );
    define( "MISSING_PERMISSION_TEMPLATE_ID", 514 );
    define( "MISSING_PERMISSION", 515 );
    define( "DUPLICATE_TEMPLATE_MENU", 516 );
    define( "INVALID_PERMISSION_TEMPLATE_MENU", 517 );
    define( "MISSING_API_ID", 518 );
    define( "MISSING_PERMISSION_TEMPLATE_MENU_API_ID", 519 );
    define( "MISSING_PERMISSION_TEMPLATE_MENU_ID", 520 );
    define( "DUPLICATED_PERMISSION_TEMPLATE_MENU_API", 521 );
    define( "INVALID_API_ID", 518 );
    define( "INVALID_PERMISSION_TEMPLATE_ID", 519);

    /**
     *  Purchase orders 600-699
     */

    // Common 600-609

    // PurchaseOrders - create & modify Currency 610-619
    define( "MISSING_CURRENCYNAME", 610 );
    define( "INCORRECT_CURRENCYNAME", 611 );
    define( "DUPLICATED_CURRENCYNAME", 612 );
    define( "MISSING_CURRENCYID", 613 );
    define( "INCORRECT_CURRENCYID", 614 );

    // PurchaseOrders - create & modify Payment Terms 620-629
    define( "MISSING_PAYMENTTERMSDESCRIPTION", 620 );
    define( "DUPLICATED_PAYMENTTERMSDESCRIPTION", 621 );
    define( "MISSING_PAYMENTTERMSID", 622 );
    define( "INCORRECT_PAYMENTTERMSID", 623 );
    
    // PurchaseOrders - create & modify Purchase Order 630-639
    define( "MISSING_PURCHASEORDERCODE", 630 );
    define( "INCORRECT_PURCHASEORDERCODE", 631 );
    define( "DUPLICATED_PURCHASEORDERCODE", 632 );
    define( "MISSING_PURCHASEORDERID", 633 );
    define( "INCORRECT_PURCHASEORDERID", 634 );

    // PurchaseOrders - create & modify Purchase Order Sku 640-649
    define( "MISSING_PURCHASEORDERSKUID", 641 );
    define( "INCORRECT_PURCHASEORDERSKUID", 642 );

    /**
     *  Warehouses 700-799
     */

    // Common 700-709

    // Warehouses - create & modify Warehouse 710-719
    define( "MISSING_WAREHOUSENAME", 710 );
    define( "INCORRECT_WAREHOUSENAME", 711 );
    define( "DUPLICATED_WAREHOUSENAME", 712 );
    define( "MISSING_WAREHOUSEID", 713 );
    define( "INCORRECT_WAREHOUSEID", 714 );

    /**
     *  Inventories 800-899
     */

    // Common 800-809

    // Inventories - create & modify Inventory 810-819
    define( "MISSING_LOT", 810 );
    define( "INCORRECT_LOT", 811 );
    define( "DUPLICATED_LOT", 812 );
    define( "MISSING_INVENTORYID", 813 );
    define( "INCORRECT_INVENTORYID", 814 );
    define( "MISSING_REFERENCENUMBER", 815 );
    define( "INCORRECT_REFERENCENUMBER", 816 );

    // Inventories - create & modify Reason 820-829
    define( "MISSING_REASONNAME", 820 );
    define( "INCORRECT_REASONNAME", 821 );
    define( "DUPLICATED_REASONNAME", 822 );
    define( "MISSING_REASONID", 823 );
    define( "INCORRECT_REASONID", 824 );

    /**
     *  Goods Received Notes 900-999
     */

    // Common 900-909

    // Goods Received Notes - create & modify GRN 910-919
    define( "MISSING_GRNREFERENCENUMBER", 910 );
    define( "INCORRECT_GRNREFERENCENUMBER", 911 );
    define( "DUPLICATED_GRNREFERENCENUMBER", 912 );
    define( "MISSING_GRNID", 913 );
    define( "INCORRECT_GRNID", 914 );

    /**
     *  Define system code description here
     */

    class SystemCode {

        public $systemCode = [

			/**
             *   System
             */
            SUCCESS => [ "en" => "Success", "tc" => "成功" ],
            INVALID_REQUEST_METHOD => [ "en" => "Invalid request method", "tc" => "請求方法無效" ],
            MISSING_LANG => [ "en" => "Missing language", "tc" => "缺少語言" ],
            MISSING_TOKEN => [ "en" => "Missing token", "tc" => "沒有令牌" ],
            INVALID_TOKEN => [ "en" => "Invalid token", "tc" => "失效的令牌" ],
            FAILED_ADD_RECORD => [ "en" => "Failed to add record", "tc" => "新增記錄失敗" ],
            FAILED_UPDATE_RECORD => [ "en" => "Failed to update record", "tc" => "更新記錄失敗" ],
            FAILED_DELETE_RECORD => [ "en" => "Failed to delete record", "tc" => "刪除記錄失敗" ],
            NO_PERMISSION => [ "en" => "No permission", "tc" => "沒有權限" ],
            MISSING_SYSTEM_LANG_ID => [ "en" => "Missing language ID", "tc" => "沒有語言ID" ],
            
            /**
             *  Member
             */

            // Common
            MISSING_EMAIL => [ "en" => "Missing email address", "tc" => "沒有電郵地址" ],
            MISSING_PASSWORD => [ "en" => "Missing password", "tc" => "沒有密碼" ],
            MISSING_USER_ID => [ "en" => "Missing user ID", "tc" => "沒有user ID" ],

			// Login
			LOGIN_FAIL => [ "en" => "Login fail", "tc" => "登入失敗" ],
            USER_DISABLED => [ "en" => "User disabled", "tc" => "失效用戶" ],
            USER_DOESNT_EXIST => [ "en" => "User doesn't exist", "tc" => "沒有這個用戶" ],
            INCORRECT_PASSWORD => [ "en" => "Incorrect password", "tc" => "密碼錯誤" ],
            USER_LOGIN_ALREADY => [ "en" => "User login already", "tc" => "用戶已經登入" ],

            // Create
            MISSING_USERNAME => [ "en" => "Missing username", "tc" => "沒有用戶名稱" ],
            DUPLICATED_EMAIL => [ "en" => "Duplicated email address", "tc" => "電郵地址已被其他人使用" ],
            DUPLICATED_USERNAME => [ "en" => "Duplicated username", "tc" => "用戶名稱已被其他人使用" ],


            /**
             *  Products
             */

            // Common

            // Create & Modify brand
            MISSING_BRANDNAME => [ "en" => "Missing brand name", "tc" => "沒有品牌名稱" ],
            INCORRECT_BRANDNAME => [ "en" => "Incorrect brand name", "tc" => "品牌名稱錯誤" ],
            DUPLICATED_BRANDNAME => [ "en" => "Duplicated brand name", "tc" => "品牌名稱已被其他人使用" ],
            MISSING_BRANDID => [ "en" => "Missing brand ID", "tc" => "沒有品牌ID" ],
            INCORRECT_BRANDID => [ "en" => "Incorrect brand ID", "tc" => "品牌ID錯誤" ],
            
            // Create & Modify category
            MISSING_CATEGORYNAME => [ "en" => "Missing category name", "tc" => "沒有類別名稱" ],
            INCORRECT_CATEGORYNAME => [ "en" => "Incorrect category name", "tc" => "類別名稱錯誤" ],
            DUPLICATED_CATEGORYNAME => [ "en" => "Duplicated category name", "tc" => "類別名稱已被其他人使用" ],
            MISSING_CATEGORYID => [ "en" => "Missing category ID", "tc" => "沒有類別ID" ],
            INCORRECT_CATEGORYID => [ "en" => "Incorrect category ID", "tc" => "類別ID錯誤" ],
            MISSING_PARENTID => [ "en" => "Missing parent category ID", "tc" => "沒有父承類別ID" ],

            // Create & Modify unit
            MISSING_UNITNAME => [ "en" => "Missing unit name", "tc" => "沒有單位名稱" ],
            INCORRECT_UNITNAME => [ "en" => "Incorrect unit name", "tc" => "單位名稱錯誤" ],
            DUPLICATED_UNITNAME => [ "en" => "Duplicated unit name", "tc" => "單位名稱已被其他人使用" ],
            MISSING_UNITID => [ "en" => "Missing unit ID", "tc" => "沒有單位ID" ],
            INCORRECT_UNITID => [ "en" => "Incorrect unit ID", "tc" => "單位ID錯誤" ],
           
            // Create & Modify supplier
            MISSING_SUPPLIERNAME => [ "en" => "Missing supplier name", "tc" => "沒有供應商名稱" ],
            INCORRECT_SUPPLIERNAME => [ "en" => "Incorrect supplier name", "tc" => "供應商名稱錯誤" ],
            DUPLICATED_SUPPLIERNAME => [ "en" => "Duplicated supplier name", "tc" => "供應商名稱已被其他人使用" ],
            MISSING_SUPPLIERID => [ "en" => "Missing supplier ID", "tc" => "沒有供應商ID" ],
            INCORRECT_SUPPLIERID => [ "en" => "Incorrect supplier ID", "tc" => "供應商ID錯誤" ],

            // Create & Modify supplier contact
            MISSING_SUPPLIERCONTACTNAME => [ "en" => "Missing supplier contact name", "tc" => "沒有供應商聯絡人名稱" ],
            INCORRECT_SUPPLIERCONTACTNAME => [ "en" => "Incorrect supplier contact name", "tc" => "供應商聯絡人名稱錯誤" ],
            MISSING_SUPPLIERCONTACTEMAIL => [ "en" => "Missing supplier contact email", "tc" => "沒有供應商聯絡人電郵" ],
            INCORRECT_SUPPLIERCONTACTEMAIL => [ "en" => "Incorrect supplier contact email", "tc" => "供應商聯絡人電郵錯誤" ],
            DUPLICATED_SUPPLIERCONTACTEMAIL => [ "en" => "Duplicated supplier contact email", "tc" => "供應商聯絡人電郵已被其他人使用" ],
            MISSING_SUPPLIERCONTACTID => [ "en" => "Missing supplier contact ID", "tc" => "沒有供應商聯絡人ID" ],
            INCORRECT_SUPPLIERCONTACTID => [ "en" => "Incorrect supplier contact ID", "tc" => "供應商聯絡人ID錯誤" ],

            // Create & Modify product
            MISSING_PRODUCTNAME => [ "en" => "Missing product name", "tc" => "沒有產品名稱" ],
            INCORRECT_PRODUCTNAME => [ "en" => "Incorrect product name", "tc" => "產品名稱錯誤" ],
            DUPLICATED_PRODUCTNAME => [ "en" => "Duplicated product name", "tc" => "產品名稱已被其他人使用" ],
            MISSING_PRODUCTCODE => [ "en" => "Missing product code", "tc" => "沒有產品代碼" ],
            INCORRECT_PRODUCTCODE => [ "en" => "Incorrect product code", "tc" => "產品代碼錯誤" ],
            DUPLICATED_PRODUCTCODE => [ "en" => "Duplicated product code", "tc" => "產品代碼名稱已被其他人使用" ],
            MISSING_PRODUCTID => [ "en" => "Missing product ID", "tc" => "沒有產品ID" ],
            INCORRECT_PRODUCTID => [ "en" => "Incorrect product ID", "tc" => "產品ID錯誤" ],

            // Create & Modify SKU Unit
            MISSING_SKUUNITNAME => [ "en" => "Missing sku unit name", "tc" => "沒有SKU單位名稱" ],
            INCORRECT_SKUUNITNAME => [ "en" => "Incorrect sku unit name", "tc" => "SKU單位名稱錯誤" ],
            DUPLICATED_SKUUNITNAME => [ "en" => "Duplicated sku unit name", "tc" => "SKU單位名稱已被其他人使用" ],
            MISSING_SKUUNITID => [ "en" => "Missing sku unit ID", "tc" => "沒有SKU單位ID" ],
            INCORRECT_SKUUNITID => [ "en" => "Incorrect sku unit ID", "tc" => "SKU單位ID錯誤" ],

            // Create & Modify SKU 
            MISSING_SKUCODE => [ "en" => "Missing sku code", "tc" => "沒有SKU代碼" ],
            INCORRECT_SKUCODE => [ "en" => "Incorrect sku code", "tc" => "SKU代碼錯誤" ],
            DUPLICATED_SKUCODE => [ "en" => "Duplicated sku code", "tc" => "SKU代碼已被其他人使用" ],
            MISSING_SKUID => [ "en" => "Missing sku ID", "tc" => "沒有SKU ID" ],
            INCORRECT_SKUID => [ "en" => "Incorrect sku ID", "tc" => "SKU ID錯誤" ],

            /**
             *  Purchase orders
             */

            // Common

            // Create & Modify currency
            MISSING_CURRENCYNAME => [ "en" => "Missing currency name", "tc" => "沒有貨幣名稱" ],
            INCORRECT_CURRENCYNAME => [ "en" => "Incorrect currency name", "tc" => "貨幣名稱錯誤" ],
            DUPLICATED_CURRENCYNAME => [ "en" => "Duplicated currency name", "tc" => "貨幣名稱已被其他人使用" ],
            MISSING_CURRENCYID => [ "en" => "Missing currency ID", "tc" => "沒有貨幣ID" ],
            INCORRECT_CURRENCYID => [ "en" => "Incorrect currency ID", "tc" => "貨幣ID錯誤" ],
           
            // Create & Modify payment terms
            MISSING_PAYMENTTERMSDESCRIPTION => [ "en" => "Missing payment terms description", "tc" => "沒有付款條件內容" ],
            DUPLICATED_PAYMENTTERMSDESCRIPTION => [ "en" => "Duplicated payment terms description", "tc" => "付款條件內容已被其他人使用" ],
            MISSING_PAYMENTTERMSID => [ "en" => "Missing payment terms ID", "tc" => "沒有付款條件ID" ],
            INCORRECT_PAYMENTTERMSID => [ "en" => "Incorrect payment terms ID", "tc" => "付款條件ID錯誤" ],

            // Create & Modify purchase order
            MISSING_PURCHASEORDERCODE => [ "en" => "Missing purchase order code", "tc" => "沒有採購訂單代碼" ],
            INCORRECT_PURCHASEORDERCODE => [ "en" => "Incorrect purchase order code", "tc" => "採購訂單件代碼錯誤" ],
            DUPLICATED_PURCHASEORDERCODE => [ "en" => "Duplicated purchase order code", "tc" => "採購訂單代碼已被其他人使用" ],
            MISSING_PURCHASEORDERID => [ "en" => "Missing purchase order ID", "tc" => "沒有採購訂單ID" ],
            INCORRECT_PURCHASEORDERID => [ "en" => "Incorrect purchase order ID", "tc" => "採購訂單件ID錯誤" ],

            // Create & Modify purchase order SKU 
            MISSING_PURCHASEORDERSKUID => [ "en" => "Missing purchase order sku ID", "tc" => "沒有採購訂單SKU ID" ],
            INCORRECT_PURCHASEORDERSKUID => [ "en" => "Incorrect purchase order sku ID", "tc" => "採購訂單件SKU ID錯誤" ],

            /**
             *  System menu
             */

            // Common

			// Create menu
            MISSING_PARENT_MENU_ID => [ "en" => "Missing parent menu ID", "tc" => "沒有Parent menu ID" ],
			MISSING_ICON => [ "en" => "Missing icon", "tc" => "沒有Icon" ],
            MISSING_NAME => [ "en" => "Missing name", "tc" => "沒有選單名稱" ],
            MISSING_ROUTE => [ "en" => "Missing route", "tc" => "沒有Route" ],
            INCORRECT_NAME_FORMAT => [ "en" => "Name should be in json format", "tc" => "name必須是Json格式" ],
            MISSING_NAME_EN => [ "en" => "Missing english name", "tc" => "沒有英文name" ],
            MISSING_NAME_TC => [ "en" => "Missing traditional chinese name", "tc" => "沒有繁體文name" ],

            /**
             *  Permission template
             */

            // Common

			// Create menu
            MISSING_PERMISSION_TEMPLATE_NAME => [ "en" => "Missing permission template name", "tc" => "沒有權限模版名稱" ],
            TEMPLATE_NAME_NOT_JSON => [ "en" => "Template name not in Json format", "tc" => "模版名稱不是Json格式" ],
            MISSING_TEMPLATE_NAME_EN => [ "en" => "Missing english permission template name ", "tc" => "沒有權限模版英文名稱" ],
            MISSING_TEMPLATE_NAME_TC => [ "en" => "Missing traditional chinese permission template name", "tc" => "沒有權限模版中文名稱" ],
            MISSING_PERMISSION_TEMPLATE_ID => [ "en" => "Missing permission template ID", "tc" => "沒有權限模版ID" ],
            MISSING_MENU_ID => [ "en" => "Missing menu ID", "tc" => "沒有選單ID" ],
            MISSING_PERMISSION => [ "en" => "Missing permission", "tc" => "沒有權限資料" ],
            DUPLICATE_TEMPLATE_MENU => [ "en" => "Duplicated template menu", "tc" => "權限模版已有該選單了" ],
            INVALID_PERMISSION_TEMPLATE_MENU => [ "en" => "Invalid permission template menu", "tc" => "沒有這權限模版選單" ],
            MISSING_API_URL => [ "en" => "Missing API url", "tc" => "沒有API網址" ],
            MISSING_API_NAME => [ "en" => "Missing API name", "tc" => "沒有API名稱" ],
            MISSING_API_ID => [ "en" => "Missing API ID", "tc" => "沒有API ID" ],
            MISSING_MENU_API_ID => [ "en" => "Missing menu API Id", "tc" => "沒有選單API ID" ],
            MISSING_PERMISSION_TEMPLATE_MENU_API_ID => [ "en" => "Missing permission template menu API Id", "tc" => "沒有權限模版選單API ID" ],
            MISSING_PERMISSION_TEMPLATE_MENU_ID => [ "en" => "Missing permission template menu Id", "tc" => "沒有權限模版選單 ID" ],
            INVALID_PERMISSION_TEMPLATE_ID => [ "en" => "Invalid permission template Id", "tc" => "沒有這權限模版 ID" ],
            DUPLICATED_PERMISSION_TEMPLATE_MENU_API => [ "en" => "Duplicated permission template menu api", "tc" => "重複的權限模板菜單 api" ],
            INVALID_API_ID => [ "en" => "Invalid api Id", "tc" => "沒有這API ID" ],
            /**
             *  Warehouse
             */

            // Common

            // Create & Modify warehouse
            MISSING_WAREHOUSENAME => [ "en" => "Missing warehouse name", "tc" => "沒有貨倉名稱" ],
            INCORRECT_WAREHOUSENAME => [ "en" => "Incorrect warehouse name", "tc" => "貨倉名稱錯誤" ],
            DUPLICATED_WAREHOUSENAME => [ "en" => "Duplicated warehouse name", "tc" => "貨倉名稱已被其他人使用" ],
            MISSING_WAREHOUSEID => [ "en" => "Missing warehouse ID", "tc" => "沒有貨倉ID" ],
            INCORRECT_WAREHOUSEID => [ "en" => "Incorrect warehouse ID", "tc" => "貨倉ID錯誤" ],

            /**
             *  Inventories
             */

            // Common

            // Create & Modify inventory
            MISSING_LOT => [ "en" => "Missing lot", "tc" => "沒有批次" ],
            INCORRECT_LOT => [ "en" => "Incorrect lot", "tc" => "批次錯誤" ],
            DUPLICATED_LOT => [ "en" => "Duplicated lot", "tc" => "批次已被其他人使用" ],
            MISSING_INVENTORYID => [ "en" => "Missing inventory ID", "tc" => "沒有存貨ID" ],
            INCORRECT_INVENTORYID => [ "en" => "Incorrect inventory ID", "tc" => "存貨ID錯誤" ],
            MISSING_REFERENCENUMBER => [ "en" => "Missing reference number", "tc" => "沒有參考編號" ],
            INCORRECT_REFERENCENUMBER => [ "en" => "Incorrect reference number", "tc" => "參考編號錯誤" ],

            // Create & Modify reason
            MISSING_REASONNAME => [ "en" => "Missing reason name", "tc" => "沒有原因名稱" ],
            INCORRECT_REASONNAME => [ "en" => "Incorrect reason name", "tc" => "原因名稱錯誤" ],
            DUPLICATED_REASONNAME => [ "en" => "Duplicated reason name", "tc" => "原因名稱已被其他人使用" ],
            MISSING_REASONID => [ "en" => "Missing reason ID", "tc" => "沒有原因ID" ],
            INCORRECT_REASONID => [ "en" => "Incorrect reason ID", "tc" => "原因ID錯誤" ],

            /**
             *  Goods Received Notes
             */

            // Common

            // Create & Modify grn
            MISSING_GRNREFERENCENUMBER => [ "en" => "Missing GRN reference number", "tc" => "沒有GRN參考編號" ],
            INCORRECT_GRNREFERENCENUMBER => [ "en" => "Incorrect GRN reference number", "tc" => "GRN參考編號錯誤" ],
            DUPLICATED_GRNREFERENCENUMBER => [ "en" => "Duplicated GRN reference number", "tc" => "GRN參考編號已被其他人使用" ],
            MISSING_GRNID => [ "en" => "Missing GRN ID", "tc" => "沒有GRN ID" ],
            INCORRECT_GRNID => [ "en" => "Incorrect GRN ID", "tc" => "GRN ID錯誤" ]

		];

    }

?>
