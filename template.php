<?php
    include "template/templateMeta.php";
    include "template/templateTab.php";
    include "template/templateCard.php";

    $pageId = $_REQUEST["pageId"];


    /**************************************
     **             Page                 **
     **************************************/

    // Fetch page information
    $pageSql = "
        select 
           *
        from 
            page p
                left join lingual l
                    on l.category = concat('page_', :pageId)
        where 
            p.pageId = :pageId and
            l.name = 'name'
    ";     
    $pageBindData["pageId"] = $pageId;
    $pageResult = $system->fetch($pageSql, $pageBindData);

    // Prepare meta
    $data["title"] = $pageResult[$lang];
    $meta = $utils->template($metaHtml, $data);


    /**************************************
     **             Content              **
     **************************************/

    // Fetch tabs information
    $tabSql = "
        select 
            *
        from 
            tab t
                left join lingual l
                    on l.category = concat('tab_',t.tabId)
        where
            t.pageId = :pageId and
            l.name = 'name'
    ";

    $tabBindData["pageId"] = $pageId;
    $tabResult = $system->fetchAll( $tabSql, $tabBindData );
    
    // Replace tag with data
    $tabData["tabItems"] = null;
    $tabData["tabPanelItems"] = null;
    $tabData["cards"] = null;
    $first = true;
    foreach( $tabResult as $tab ) {

        // Prepare cards data
        $cardSql = "
            select
                *
            from
                card c
                    left join lingual l
                        on l.category = concat('card_',c.cardId)
            where
                c.tabId = :tabId and
                l.name = 'name'
        ";
        $cardBinddata["tabId"] = $tab["tabId"];
        $cardResult = $system->fetchAll( $cardSql, $cardBinddata );

        // Replace {{variables}} in card template
        $tab["cards"] = null;
        foreach( $cardResult as $card ) {

            // Datatabe information
            $card["name"] = $card[$lang];
            $tab["cards"] .= $utils->template( $cardHtml, $card );

        } 
        

        // First tab is active
        $tab["active"] = "";
        if ( $first ) {

            $first = false;
            $tab["active"] = "active";

        }
        
        // Select language for name
        $tab["name"] = $tab[$lang];

        // Replace {{Varialbes}} in tab items and tab panel items tempalte
        $tabData["tabItems"] .= $utils->template( $tabItemsHtml, $tab );
        $tabData["tabPanelItems"] .= $utils->template( $tabPanelItemsHtml, $tab );
    }

    // Finaly replace {{variables}} in tab template
    $content = $utils->template( $tabHtml, $tabData );

?>