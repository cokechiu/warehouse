<?php

    class System extends Dbhelper {

        // Compulsary variable
        public $debug = false;		// Boolean, show debug information in response
        public $lang = "en";		// Language setting. [ en / tc ]
        public $return = [];		// Store return information.
        public $debugInfo = [];		// Store debug information
        public $action = "";		// For logging, state what are you doing. e.g. "User login".....etc
        public $checking = [];  	// Check user permission before execute api

        public function empty( $data, $testCase ) {

            foreach( $testCase as $name => $errorCode) {
                
                $result = empty($data[$name]);
                $result = ( $data[$name] == null || $data[$name] == "" );

                $this->code($errorCode);
                return $this->return();

            }
            

        }
        
        public function log( $action, $log ) {
			
            global $headers;
            $username = "Login api";
            $email = "Login api";
    
            // Get user information
            if ( 
    
                $_SERVER["REQUEST_URI"] != "/api/user/userLogin.php" &&
                $_SERVER["REQUEST_URI"] != "/api/global.php" ) {
    
                $getUserSql = "select * from user where token = :token";
                $getUserBindData["token"] = $headers["token"];
                
                if ( $this->count($getUserSql, $getUserBindData) > 0 ) {
                    
                    $user = $this->fetch($getUserSql, $getUserBindData);
                    $username = $user["username"];
                    $email = $user["email"];
    
                } else {
    
                    $username = "Unknown";
                    $email = "Unknown";
    
                }
                
    
            }
    
            $logSql = "
                insert into systemSqlLog
                    ( action, log, ip, username, email, date )
                    values
                    ( :action, :log, :ip, :username, :email, now() )
            ";
    
            $logBindData["action"] = $action;
            $logBindData["log"] = $log;
            $logBindData["ip"] = ( empty($headers["ip"]) ) ? "Unknown" : $headers["ip"];;
            $logBindData["username"] = $username;
            $logBindData["email"] = $email;
            $this->sql( $logSql, $logBindData );
    
        }
    
        public function rowReorder( $data ) {
    
            // Reset dorder for testing
            $this->sql("update api set dorder = apiId");
    
            $action = "Re-ordering records";
    
            $result["rawData"] = $data;
            extract( $data );
    
            $tableName = addslashes($tableName);
            $targetPk = addslashes($targetPk);
            $destinationPk = addslashes($destinationPk);
    
            // Get destination reocrd's dorder value
            $targetSql = "select * from ".$tableName." where ".$tableName."Id = ".$targetPk;
            $targetBindData["targetPk"] = $targetPk;
            $targetResult = $this->fetch( $targetSql, $targetBindData );
    
            $destinationSql = "select * from ".$tableName." where ".$tableName."Id = ".$destinationPk;
            $destinationBindData["destinationPk"] = $destinationPk;
            $destinationResult = $this->fetch( $destinationSql, $destinationBindData );
    
            // Step - 1,  Below target reocord's droder set to 0
            $rowReorderSql = "update ".$tableName." set dorder = -1 where ".$tableName."Id =:targetPk;";
            $debug["sql-1"] = "update ".$tableName." set dorder = -1 where ".$tableName."Id =:targetPk;";
    
            // Step - 2,  All record between target and destination dorder +1 / -1
            if ( $targetResult["dorder"] > $destinationResult["dorder"] ) {
    
                $rowReorderSql .= "update ".$tableName." set dorder = dorder + 1 where dorder >= :destinationDorder and dorder < :targetDorder;";
                $debug["sql-2"] = "update ".$tableName." set dorder = dorder + 1 where dorder >= :destinationDorder and dorder < :targetDorder;";
    
            } else {
    
                $rowReorderSql .= "update ".$tableName." set dorder = dorder - 1 where dorder > :targetDorder and dorder <= :destinationDorder;";
                $debug["sql-2"] = "update ".$tableName." set dorder = dorder - 1 where dorder > :targetDorder and dorder <= :destinationDorder;";
    
            }
            
            // Step - 3, Set target dorder equal to destination dorder
            $rowReorderSql .= "update ".$tableName." set dorder = :destinationDorder where ".$tableName."Id = :targetPk;";
            $debug["sql-3"] = "update ".$tableName." set dorder = :destinationDorder where ".$tableName."Id = :targetPk;";
    
            $rowReorderBindData["targetPk"] = $targetPk;
            $rowReorderBindData["targetDorder"] = $targetResult["dorder"];
            $rowReorderBindData["destinationDorder"] = $destinationResult["dorder"];
            $this->sql($rowReorderSql, $rowReorderBindData);
    
            $result["debugInfo"] = $debug;
            $result["BindData"] = $rowReorderBindData;
    
            return $result;
    
        }

        public function code($code) {
			
			$temp["code"] = $code;
			$temp["message"] = $this->systemCode[$code][$this->lang]; 

			// API result statue always on top
			$this->return = array_merge( $temp, $this->return );

		}

		public function return() {

			if ($this->debug) {

				$this->debugInfo["checking"] = $this->checking;
				$this->return["debug"] = $this->debugInfo;

			}
			$this->log( $this->action, addslashes(json_encode($this->debugInfo)) );
			return $this->return;

		}

        public function clearTimeoutToken() {

			$this->action = "Clear timeout token ";

			// Timeout in minutes
			$timeoutDurationSeconds = 120 * 60;
			$timeoutSeconds = time() - $timeoutDurationSeconds;

			$clearSql = "update user set token = null where token is not null and lastAction < '".date( "Y-m-d, G:i:s", $timeoutSeconds)."'";
			$this->checking["clearTimeoutToken"]["timeoutMinutes"] = $timeoutDurationSeconds/60;
			$this->checking["clearTimeoutToken"]["sql"] = $clearSql;
			$this->checking["clearTimeoutToken"]["now"] = date("Y-m-d, G:i:s");
			$this->sql($clearSql);

		}

		public function updateLastAction($data) {

			$this->action = "Update user last action time";
			$sql = "update user set lastAction = now() where token = :token";
			$sqlBindData["token"] = $data["token"];
			$this->checking["updateLastAction"]["sql"] = $sql;
			$this->checking["updateLastAction"]["bindData"] = $sqlBindData;
			$this->sql($sql, $sqlBindData);

		}

        public function checkPermission($headers) {

            $this->action = "Check user permission";

            $selfUrl = $_SERVER["REQUEST_URI"];
            $this->checking["selfUrl"] = $selfUrl;

            $excludedApi = (
                $selfUrl != "/api/menu/menuApiListAll.php" &&
                $selfUrl != "/api/user/userLogin.php" &&
                $selfUrl != "/api/global.php" );

            // API require a user token to execute
            if ( empty($headers["token"]) && $excludedApi ) {
                
                $this->code(MISSING_TOKEN);
                return $this->return();

            }

            if ( $excludedApi) {
                
                // Validate token
                $validateTokenSql = "select * from user where token = :token";
                $validateTokenBindData["token"] = $headers["token"];
                $this->checking["validateToken"]["sql"] = $validateTokenSql;
                $this->checking["validateToken"]["bindData"] = $validateTokenBindData;

                // Invalid token
                if ( $this->count($validateTokenSql, $validateTokenBindData) == 0 ) {

                    $this->code(INVALID_TOKEN);
                    return $this->return();
                    
                }

                // Refresh user last action time
                $updateLastAction["token"] = $validateTokenBindData["token"];
                $this->updateLastAction( $updateLastAction );

                // Check permission
                $checkPermissionSql = "
                    select 
                        * 
                    from 
                        user u 
                            left join permissionTemplate t 
                                on t.permissionTemplateId = u.permissionTemplateId 
                            left join permissionTemplateMenu tm 
                                on tm.permissionTemplateId = t.permissionTemplateId 
                            left join permissionTemplateMenuApi tmi 
                                on tmi.permissionTemplateMenuId = tm.permissionTemplateMenuId 
                            left join api a 
                                on a.apiId = tmi.apiId 
                    where 
                        u.token = :token and 
                        a.apiUrl = :apiUrl
                ";
                $checkPermissionBindData["token"] = $headers["token"];
                $checkPermissionBindData["apiUrl"] = $selfUrl; 
                $this->checking["checkPermission"]["sql"] = $this->cleanSql($checkPermissionSql);
                $this->checking["checkPermission"]["bindData"] = $checkPermissionBindData;

                // if ( $member->count( $checkPermissionSql, $checkPermissionBindData ) == 0 ) {
                    
                // 	$this->code(NO_PERMISSION);
                // 	return $this->return();

                // }
                
            } 
            
            return [ "code"=> SUCCESS ];

        }

    }



?>