<?php

    $cardHtml = '
        <div class="{{col}}">
            <div class="row">
                <div class="col">
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">{{name}}</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                {{datatables}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    ';

?>