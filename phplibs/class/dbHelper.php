<?php

	class DbHelper extends SystemCode {

		public $db;

		function sqlBind($sql, $data) {

			// initiate a query
			$query = $this->db->raw();

			// Bind data
			foreach( $data as $key => $value ) {

				// Set data type
				$dataType = "";
				switch( $value ) {
					
					case is_bool($value):
						$dataType = PDO::PARAM_BOOL;
						break;
					case is_float($value):
						$dataType = PDO::PARAM_STR;
						break;
					case is_numeric($value):
						$dataType = PDO::PARAM_INT;
						break;
					
					case is_array($value):
						$dataType = PDO::PARAM_STR;
						$value = json_encode($value,JSON_NUMERIC_CHECK);
						break;

				}

				$query->bind(":".$key, $value, $dataType);

			}

			// execute the sql and return result object
			$result = $query->exec($sql);
			
			return $result;

		}

		public function fetch($sql, $data = null) {
			$result = ( is_null($data) ) ? 
				$this->db->sql($sql):
				$this->sqlBind($sql, $data);
			
			return $result->fetch();
		}

		public function fetchAll($sql, $data = null) {
			$result = ( is_null($data) ) ? 
				$this->db->sql($sql):
				$this->sqlBind($sql, $data);
			
			return $result->fetchAll();
		}

		public function count($sql, $data = null) {
			$result = ( is_null($data) ) ? 
				$this->db->sql($sql):
				$this->sqlBind($sql, $data);
			
			return $result->count();
		}

		public function sql($sql, $data = null) {
			return ( is_null($data) ) ? 
				$this->db->sql($sql):
				$this->sqlBind($sql, $data);
		}

		public function cleanSql($sql) {
			// return $sql;
			return str_replace( array( "\r", "\t", "\n" ), array( "", "", "" ), $sql);
		}

	}

?>