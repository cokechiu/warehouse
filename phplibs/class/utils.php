<?php

    class Utils {

        public function printr( $data ) {

            echo "<script>";
            echo "console.log(".json_encode($data).")";
            echo "</script>";

        }

        public function template( $html, $data ) {

            foreach ( $data as $search => $replace ) {

                $html = str_replace("{{".$search."}}", $replace, $html);

            }

            return $html;

        }

    }

?>