<?php 
	function generatePdf($client, $clientPaints, $outputPath) {
$linkDef = '<a href="{{url}}">{{name}}</a>';
$websiteUrl = $client["website1"];
$temp = ( strpos($websiteUrl,"http://")===false && strpos($websiteUrl,"https://")===false ) ? "http://".$websiteUrl : $websiteUrl;
$link1 = ( empty($websiteUrl) ) ? $client["activity1"] : str_replace("{{name}}", $client["activity1"], str_replace("{{url}}", $temp, $linkDef));

$websiteUrl = $client["website2"];
$temp = ( strpos($websiteUrl,"http://")===false && strpos($websiteUrl,"https://")===false ) ? "http://".$websiteUrl : $websiteUrl;
$link2 = ( empty($websiteUrl) ) ? $client["activity2"] : str_replace("{{name}}", $client["activity2"], str_replace("{{url}}", $temp, $linkDef));

$websiteUrl = $client["website3"];
$temp = ( strpos($websiteUrl,"http://")===false && strpos($websiteUrl,"https://")===false ) ? "http://".$websiteUrl : $websiteUrl;
$link3 = ( empty($websiteUrl) ) ? $client["activity3"] : str_replace("{{name}}", $client["activity2"], str_replace("{{url}}", $temp, $linkDef));

$data = [];
$client["link1"] = $link1;
$client["link2"] = $link2;
$client["link3"] = $link3;
$data["client"] = $client;

$data["clientPaint"] = $clientPaints;

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

ini_set('memory_limit', '256M');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);


$HEADER_TITLE = "　參賽者資料";

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));
// set default footer data
$pdf->setFooterData(array(0,64,0), array(0,0,0));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);
$fontname = TCPDF_FONTS::addTTFfont('../fonts/wqy_microhei.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont($fontname, '', 10);
$pdf->setHeaderFont(Array($fontname, '', 10));



// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();
if ($client['profile']!=""){
	if (file_exists("../../../".$client['profile'])) {
		if (getimagesize("../../../".$client['profile'])) {
			$info = getimagesize("../../../".$client['profile']);
			$img = [];
			$img["width"] = $info[0];
			$img["height"] = $info[1];
			$container = [];
			$container["width"] = 700;
			$container["height"] = 500;
			$resize = resize($img, $container);
			$html = <<<EOT
			<div width="700" height="500" style="text-align: center">
				<img src="../../../{$client['profile']}" width="{$resize['width']}" height="{$resize['height']}">
			</div>
EOT;
			$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
		}
	}
}

//Escape html character
$cIntroTc  = htmlentities($client["introTc"]);
$cIntroEn  = htmlentities($client["introEn"]);
$cAddress1 = htmlentities($client["address1"]);
$cAddress2 = htmlentities($client["address2"]);
$cAddress2 = htmlentities($client["address2"]);

// Set some content to print
$html = <<<EOT
<html> 
	<head>
	<style>
		table {
			border-spacing :10px;
		}
	</style>
	</head>
	<body>
		<h1>個人資料</h1>
		<table style="width:100%; font-size: 12pt">
			<tr>
				<td width="20%">稱謂</td>
				<td width="80%">{$client['title']}, {$client['firstName']} {$client['lastName']} </td> 
			</tr>
			<tr>
				<td width="20%">姓名全名</td>
				<td width="80%">{$client['fullName']}</td> 
			</tr>
			<tr>
				<td width="20%">國家</td>
				<td width="80%">{$client['nameTc']}</td> 
			</tr>
			<tr>
				<td width="20%">中文個人簡介</td>
				<td width="80%">{$cIntroTc}</td> 
			</tr>
			<tr>
				<td width="20%">英文個人簡介</td>
				<td width="80%">{$cIntroEn}</td> 
			</tr>
			<tr>
				<td width="20%">出生日期</td>
				<td width="80%">{$client['birthday']}</td> 
			</tr>
			<tr>
				<td width="20%">聯絡電話一</td>
				<td width="80%">{$client['contactAreaCode']} - {$client['contact']}</td> 
			</tr>
			<tr>
				<td width="20%">聯絡電話二</td>
				<td width="80%">{$client['otherContactAreaCode']} - {$client['otherContact']}</td> 
			</tr>
			<tr>
				<td width="20%">電郵</td>
				<td width="80%">{$client['email']}</td> 
			</tr>
			<tr>
				<td width="20%">聯絡地址</td>
				<td width="80%">{$cAddress1} {$cAddress2}</td> 
			</tr>
			<tr>
				<td width="20%">聯絡語言</td>
				<td width="80%">{$client['language']}</td> 
			</tr>
		</table>
		<h1>活動一</h1>
		<table style="width:100%; font-size: 12pt">
			<tr>
				<td width="20%">名稱</td>
				<td width="80%">$link1</td> 
			</tr>
			<tr>
				<td width="20%">主辦單位</td>
				<td width="80%">{$client['host1']}</td> 
			</tr>
			<tr>
				<td width="20%">參加年份</td>
				<td width="80%">{$client['year1']}</td> 
			</tr>
		</table>
		<h1>活動二</h1>
		<table style="width:100%; font-size: 12pt">
			<tr>
				<td width="20%">名稱</td>
				<td width="80%">$link2</td> 
			</tr>
			<tr>
				<td width="20%">主辦單位</td>
				<td width="80%">{$client['host2']}</td> 
			</tr>
			<tr>
				<td width="20%">參加年份</td>
				<td width="80%">{$client['year2']}</td> 
			</tr>
		</table>
		<h1>活動三</h1>
		<table style="width:100%; font-size: 12pt">
			<tr>
				<td width="20%">名稱</td>
				<td width="80%">$link3</td> 
			</tr>
			<tr>
				<td width="20%">主辦單位</td>
				<td width="80%">{$client['host3']}</td> 
			</tr>
			<tr>
				<td width="20%">參加年份</td>
				<td width="80%">{$client['year3']}</td> 
			</tr>
		</table>
	</body>
</html>
EOT;

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

$pdf->AddPage();

$nos = [ "一", "二", "三" ];
foreach($clientPaints as $key => $clientPaint) {
	if ($clientPaint["paint"] !="") {
		if (file_exists("../../../".$clientPaint["paint"])) {
			if (getimagesize("../../../".$clientPaint["paint"])) {
				$info = getimagesize("../../../".$clientPaint["paint"]);
				$img = [];
				$img["width"] = $info[0];
				$img["height"] = $info[1];
				$container = [];
				$container["width"] = 700;
				$container["height"] = 500;
				$resize = resize($img, $container);
				$pdf->AddPage();
				$html = <<<EOT
				<div width="700" height="500" style="text-align: center">
					<img src="../../../{$clientPaint['paint']}" width="{$resize['width']}" height="{$resize['height']}"></img>
				</div>
EOT;
				$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
			}
		}
	}
	
	//Escape html character
	$cpTitle     = htmlentities($clientPaint["title"]);
	$cMaterial   = htmlentities($clientPaint["material"]);
	$cpIntroTc   = htmlentities($clientPaint["introTc"]);
	$cpIntroEn   = htmlentities($clientPaint["introEn"]);
	$cpExhibited = htmlentities($clientPaint["exhibited"]);

	// $pdf->AddPage();
	$html = <<<EOT
		<html> 
			<head>
			<style>
				table {
					border-spacing :10px;
				}
			</style>
			</head>
			<body>
				<h1>作品{$nos[$key]}</h1>
				<table style="width:100%; font-size: 12pt">
					<tr>
						<td width="20%">標題</td>
						<td width="80%">{$cpTitle}</td> 
					</tr>
					<tr>
						<td width="20%">創作年份</td>
						<td width="80%">{$clientPaint["year"]}</td> 
					</tr>
					<tr>
						<td width="20%">物料</td>
						<td width="80%">{$cMaterial}</td> 
					</tr>
					<tr>
						<td width="20%">作品簡介 - 中文</td>
						<td width="80%">{$cpIntroTc}</td> 
					</tr>
					<tr>
						<td width="20%">作品簡介 - 英文</td>
						<td width="80%">{$cpIntroEn}</td> 
					</tr>
					<tr>
						<td width="20%">畫心尺寸(厘米)</td>
						<td width="80%">{$clientPaint["width"]} x {$clientPaint["height"]}</td> 
					</tr>
					<tr>
						<td width="20%">展覽全稱</td>
						<td width="80%">{$cpExhibited}</td> 
					</tr>
				</table>
			</body>
		</html>
EOT;
	$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
}
// print a block of text using Write()
// $pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output($outputPath, 'F');
// $pdf->Output($clientId." - ".$client["fullName"].".pdf", 'I');
//============================================================+
// END OF FILE
//============================================================+
		
	}

?>