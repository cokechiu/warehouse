﻿<?php
	$debug = false;
	include "../phpclass/util.php";
	include "../config/configWebsite.php";

	$util = new util();
	$util ->isDirectLink();
	
	use PHPMailer\PHPMailer\PHPMailer;

	require 'vendor/autoload.php';
	
	$errorStr = "";
	
	$emailTemplateId = addslashes($_REQUEST["emailTemplateId"]);
	$sql = "select * from emailTemplate where emailTemplateId ='".$emailTemplateId."'";
	$setting = $db->sql($sql)->fetch();
	
	$folder = @$_REQUEST["folder"];
	$smtp = $setting["smtp"];
	$from = $setting["email"];
	$fromName = $setting["fromName"];
	$username = $setting["username"];
	$password = $setting["password"];
	$port = $setting["port"];

	$redirect = $setting["url"];
	$email = $setting["mailTo"];
	$subject = $setting["subject"];
	$content = $setting["content"];
	
	$request = $_REQUEST;

	$request["lang"] = ( !isset($request["lang"]) ) ? "" : "/".strtolower($request["lang"]);

	foreach($request as $key=>$value) {
		// replace markup with form data {$example}
		$subject = str_replace("{\$".$key."}",$value,$subject);
		$content = str_replace("{\$".$key."}",$value,$content);
		$email = str_replace("{\$".$key."}",$value,$email);
		$redirect = str_replace("{\$".$key."}",$value,$redirect);
	}
	
	try {
		$mail = new PHPMailer(true);
		$mail->isSMTP();
		
		$mail->CharSet	  = "UTF-8";
		$mail->Host       = $smtp;
		$mail->Port       = $port;
		// $mail->SMTPSecure = 'tls';
		$mail->SMTPAuth   = true;
		$mail->Username   = $username;
		$mail->Password   = $password;

		$mail->SetFrom($from, $fromName);
		$mail->addAddress($email);
		
		$mail->SMTPDebug   = 4;
		$mail->Debugoutput = function ($str, $level) { 
			global $errorStr;
			$errorStr .= $str."<br>";
		};
		$mail->IsHTML(true);
		// $mail->SMTPOptions = array('tls' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true)); //remove this part once you get the SSL Certificate working

		$mail->Subject = $subject;
		$mail->Body    = $content;
		$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if (!$mail->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			echo 'Message has been sent';
		}
	} catch (Exception $e) {
		// save log into database
		$sql = "insert into emailLog values ( null, '".$emailTemplateId."' ,'".addslashes($errorStr)."' ,'".json_encode($setting)."' ,'".json_encode($_REQUEST)."', '".$from."', '".$email."' ,'".$subject."' ,'".$content."' ,'".date("Y-m-d, H:i:s",time())."' )";
		$db->sql($sql);
		echo "Exception error while sending email. Please check email log at backend<br>";
		// print_r($request);
		// echo $folder.$redirect;
		die();
	}
	
	// save log into database
	$sql = "insert into emailLog values ( null, '".$emailTemplateId."' ,'No error' ,'".json_encode($setting)."' ,'".json_encode($_REQUEST)."', '".$from."', '".$email."' ,'".$subject."' ,'".$content."' ,'".date("Y-m-d, H:i:s",time())."' )";
	$db->sql($sql);
	
	if ($setting["redirect"]!="0") {
		echo "<script>location.href='".$folder.$redirect."'</script>";
	} else {
		// Disable redirect setting at backend and write debug code here
		echo "<pre>";
		print_r($_REQUEST);
		print_r($setting);
		echo "</pre>";
		echo "<hr>".$subject."<hr>".$content;
	}
?>
