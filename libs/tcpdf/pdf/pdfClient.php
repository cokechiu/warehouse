<?php
    include "../libs/datatables/media/php/DataTables.php";
    include "../phpclass/util.php";

    $util = new util();
    $util -> setDebug(false);
    
	foreach($_REQUEST as $key => $value) {
		$$key = $value;
	}
	// $clientPaintId;
	// if (!isset($clientPaintId) {
    //     die();
    // }

    $sql =  "select 
                cp.*
            from
                client c 
                    left join clientPaint as cp on cp.clientId = c.clientId 
            where 
                c.activate = '1' and c.closed = '1' and cp.clientPaintId ='".addslashes($clientPaintId)."'";

    $clientPaint = $db->sql($sql)->fetch();

    $util->printr($clientPaint);

?>
<!DOCTYPE html>
<html>
    <head>
    </head>
    <body>
        <script data-main="./js/main" src="./js/require.js"></script>
        <script>
            requirejs(["./jquery-3.4.1.min"]);
            require.config({
                baseUrl: "js",
                packages: [ "makePdf" ]
            });
            require(["makePdf"], function  (pdf){
                $.ajax({
                    url: "/client/wanfung/admin/makePdf/readImage/index.php",
                    data: "image=<?php echo $clientPaint['paint']?>",
                    complete: function(data){
                        console.log("Complete1");
                        var doc = pdf.defaultDosDefinition;
                        var content = [
                            {
                                style: "paint",
                                fit: [550, 700]
                            }
                        ];
                        doc.content = content;
                        doc.content[0].image = data.responseText;
                        // var pdfDoc =
                        pdf.createPdf(doc).download("<?php echo $clientPaint['clientPaintId']?> - <?php echo $clientPaint['title']?>.pdf",()=>{}, {});
                        // pdfDoc.open({}, window);
                    },
                    fail: function(data){
                        console.log("Fail");
                        console.log(data.responseText);
                    },
                });
            });
        </script>
    </body>
</html>