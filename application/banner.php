<?php
	include "../config/configWebsite.php";
	include adminAbsolutePath."phpclass/util.php";

	$util = new util();
	$util->setDebug(false);

	// Reset session data for links between tables
	// $_SESSION[ $website["sessionName"] ]["activitiesId"] = -1;

	// Browser title display
	$title = "Banner";
	
	// Left menu hightlight setting
	$page = "banner";
	
	// Content setting
	$section = array();
	$table = array();

	// Section
	$temp = new stdclass();
	$temp->tabList = false;
	$temp->tab = array(
		""
	);
	$section[0] = $temp;

    // activities
	// $table[ section ][ tab ][ table ]
	$temp = new stdclass();
	$temp->cardWidth = array(
		"1-1@l",
		"1-1@s"
	);	
	$temp->cardColor = "default"; 		// [ default / primary / secondary ]
	$temp->cardIcon = "file-text";
	$temp->cardTitle = "Banner"; 
	$temp->tableId = "banner";
	$temp->contentType = "table";			// [ table / php ]
	$temp->html = "";
	$temp->fields = array(
        array( "width"=>"", "name"=>"dorder" ),
		array( "width"=>"50", "name"=>"" ),
        array( "width"=>"100", "name"=>"Start" ),
        array( "width"=>"100", "name"=>"End" ),
		array( "width"=>"50", "name"=>"Image" ),
		array( "width"=>"", "name"=>"landing" ),
		array( "width"=>"50", "name"=>"Active" )
	);
	$temp->form = false;
	$temp->formId = "";
	$temp->formTab = array();
	$temp->formFields = array(
		array( "tab"=>"Common", "width"=>array("1-1@l","1-1@s"), "data"=>"activities.dorder" )	
	);
	$card[0][0][] = $temp;
	
?>
<!doctype html>

<html>
	<head>
		<?php 
			include adminAbsolutePath."meta.php"; 
			include adminAbsolutePath."commonCss.php";
			include adminAbsolutePath."pageCss.php"; 
		?>
	</head>
	<body>
		<!-- Header -->
		<?php 
			include adminAbsolutePath."header.php";
			include adminAbsolutePath."menu.php";
		?>

		<!--- Content: start -->
			<div id="content" data-uk-height-viewport="expand: true" class="uk-margin-top">
				<?php
					include adminAbsolutePath."content.php";
					echo $display;
				?>
			</div>
		<!-- Content: end -->
		
		<?php 
			include adminAbsolutePath."commonJs.php"; 
			include adminAbsolutePath."pageJs.php";
		?>
	</body>
</html>