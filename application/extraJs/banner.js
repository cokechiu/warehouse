$(document).ready(function() {
	
	var tableId = "banner";

	// Banner Image
	var thumbnailFileTable1 = "bannerImage";
	var thumbnailField1 = tableId + ".bannerImageId";

	bannerEditor = new $.fn.dataTable.Editor({
        ajax: "extraDao/"+tableId+"Dao.php",
		table: "#"+tableId,
		// template: "#bannerForm",
        fields: [ 
			{
				label: "dorder",
				name: tableId + ".dorder",
				type: "hidden",
				def: 0
			},{
				label: "Active",
				name: tableId + ".active",
				type: "select",
				options: [
					{ "label":"Yes", "value": "1" },
					{ "label":"No", "value": "0" }
				]
			},{
				label: "Start date",
				name: tableId + ".startDate",
				type: "datetime"
			},{
				label: "End date",
				name: tableId + ".endDate",
				type: "datetime"
			},{
				label: "Landing",
				name: tableId + ".landing"
			},{
				label: "Image",
				name: thumbnailField1,
				type: "upload",
				display: function ( file_id ) {
					return "<img width='100' src='" + imagePath(bannerTable.file( thumbnailFileTable1, file_id ).web_path) + "'>";
				},
				clearText: "Clear",
				noImageText: 'No Image'
			}
        ]
	});

	bannerEditor
		.on('submitSuccess',function(e, json, data, action, xhr ) {
			if ( json.timeOut ) {
				timeoutRedirect();
			}
		})
		.on('postCreate',function(e, json, data, action, xhr ) {

		})
		.on('postUpdate',function(e, json, data, action, xhr ) {

		})
		.on('postRemove',function(e, json, data, action, xhr ) {
			
		});

		
	bannerTable = $("#"+tableId).DataTable({
		dom: "Bfrtip",
		ajax: "extraDao/"+tableId+"Dao.php",
		rowReorder: {
			dataSrc: tableId+".dorder",
			editor: bannerEditor
		},
		paging: false,
		scrollY: "60vh",
		scrollX: true,
		order: [ [3,"desc"],[2,"desc"],[0,"desc"] ],
		columns: [
			{ data: tableId + ".dorder", visible: false },
			{
				render: function() {
					return "<span uk-icon='icon:move; ratio:1'></span>";
				}
			},
			{ data: tableId + ".startDate" },
			{ data: tableId + ".endDate" },
			{ 
				data: "image",
				render: function(data) {
					return ( data == null ) ? "No image" : "<img width='100' src='/" + data + "' uk-image>";
				}
			},
			{ data: tableId + ".landing" },
			{ 
				data: tableId + ".active",
				render: function(data,type) {
					if ( type == "display" ) {
						icon = (data==1) ? "check" : "close";
						return "<span uk-icon='icon:" + icon + "; ratio:1'></span>";
					} else {
						return data;
					}
				}
			}
		],
		select: true,
		buttons: [
			{ extend: "create", text: "<u>C</u>reate", key: 'c', className:"uk-button uk-button-default", editor: bannerEditor },
			{ extend: "edit", 	text: "<u>M</u>odify", key: 'm', className:"uk-button uk-button-default", editor: bannerEditor },
			{ extend: "remove", text: "<u>D</u>elete", key: 'd', className:"uk-button uk-button-default", editor: bannerEditor },
			{ 
				text: "Refresh",
				enabled: true,
				className:"uk-button uk-button-default",
				action: function(e, dt, node, config) {
					message("info",tableId+ " Refreshed",2000);
					bannerTable.ajax.reload();
				}
			}
		]
	});

	bannerTable
		.on("select",function(e, dt, data, row, meta){

		})
		.on("deselect",function(e, dt, data, row, meta){

		});
});