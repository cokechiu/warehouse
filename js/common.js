// Popup notification
function message( icon, message, time = 0 ){
    UIkit.notification({
        message: "<span class='uk-margin-small-right' uk-icon='icon: "+ icon +"; ratio: 1.6'></span> " + message,
        status: 'primary',
        pos: 'top-center',
        timeout: time
    });
}

// changing session information
function session(name, value, time = 0, handle ) {
    $.ajax({
        type: "post",
        url: "../phpclass/session.php",
        data: "name=" + name + "&value=" + value,
        dataType: "json",
        success: function (response) {
            handle(response);
        }
    });
}

function timeoutRedirect() {
    location.href="../index.php?url=" + $(".logoutBtn").attr("returnUrl");
}

// Check login status
function checkLogin(){
    setInterval(
        function() {
            console.log("Checking login status")
            $.ajax({
                url:"../api/isLoginRegular.php",
                dataType: "json",
                success: function(data){
                    console.log(data);
                    if (data.timeOut) {
                        message("info","Login timeout",0);
                        timeoutRedirect();
                    }
                }
            });
        },60000
    );
}

// Fix image path slash at the end of the path
function imagePath( path ) {
	return (path.split('')[0] == "/") ? path : "/" + path;
}

// Debug datatables
function debug(){
    var n = document.createElement('script');
    n.setAttribute('language', 'JavaScript');
    n.setAttribute('src', 'https://debug.datatables.net/debug.js');
    document.body.appendChild(n);
}

/**************************************/ 
/**              Editor              **/
/**************************************/

function editorDorder( tableId, displayName='dorder' ) {
    return {
        label: displayName,
        name: tableId + ".dorder",
        type: "hidden",
        def: 0
    }
}

function editorActive( tableId, displayName='Active' ) {
    return {
        label: displayName,
        name: tableId + ".active",
        type: "select",
        options: [
            { "label":"Yes", "value": "1" },
            { "label":"No", "value": "0" },
        ]
    }
}

/**************************************/ 
/**              Table               **/
/**************************************/


$(document)
    .ready( function() {

        $(".logoutBtn").on("click",function(){
            $.ajax({
                url: "../api/logout.php",
                dataType: "json",
                complete: function(data) {
                    console.log(data);
                    location.href = "../index.php?url=" + $(".logoutBtn").attr("returnUrl");
                }
            })
        });

        $(".menuItem").on("click", function(event) {
            event.preventDefault();
            $.ajax({
                type: "post",
                url: "../phpclass/session.php",
                data: "name=menuItemId&value=" + $(this).attr("menuItemId"),
                dataType: "json",
                location: $(this).attr("location"),
                target: $(this).attr("target"),
                complete: function (response) {
                    (this.target=="") ? location.href = this.location : window.open(this.location);
                }
            });
        });

        // Start check login status
        checkLogin();

       
    })
    .on("keypress",function(event){
        console.log(event.keyCode);
        if (event.keyCode == 26) {
            console.log("Ctrl + z pressed");
            $.ajax({
                url: "../system/sessionsModal.php",
                complete: function(data) {
                    UIkit.modal.dialog(data.responseText);
                    $(".uk-modal-dialog").draggable();
                }
            })
        }
    })