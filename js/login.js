$(document).ready( function() {
				
    if (useFreeImageSource==1) {
        var start = Math.floor(Math.random() * 900);
        var id = start;
    } else {
        id = 1;
        var max = backgroundList.length;
    }

    $(".loginBackground").css( "background-image", "url('" + imageSource() + "'" );
    $(".hideImage").attr("src", imageSource());

    function changeBg() {

        setTimeout(function() {
 
            var el = $(".loginBackground");
            src = imageSource();

            // Start changing photo
            el.fadeOut("slow", ()=>{

                el.css( "background-image", "url('" + $(".hideImage").attr("src") + "'" );
                $(".hideImage").attr("src", src);
                el.fadeIn("slow");

            });

            changeBg();

        }, 10000);

    };

    function imageSource(){
        if (useFreeImageSource==1) {
            id = ( id == (start + 100) ) ? start : (id + 1);
            scale = 0.8;
            width = Math.round( $(window).width() * scale );
            height = Math.round( $(window).height() * scale );
            return "https://picsum.photos/id/" + id + "/" + width + "/" + height + "/";
        } else {
            id = ( (id+1)==max ) ? 2 : (id+1);
            //console.log(id);
            //console.log(backgroundList[id]);
            return loginBackgroundImageFolder + backgroundList[id];
        }
    }

    function getImg() {
        canvas = document.createElement('canvas');
        context = canvas.getContext('2d');
        img = document.getElementById('hideImage');
        canvas.width = img.width;
        canvas.height = img.height;
        context.drawImage(img, 0, 0 );
        myData = context.getImageData(0, 0, img.width, img.height);
        return myData;
    }

    changeBg();

    $("#loginForm").on("submit",function(event){
        event.preventDefault();
        formData = $(this).serialize();

        console.log("Submit button clicked");
        console.log(formData);
        
        $.ajax({
            url: "api/checkLogin.php",
            data: formData,
            method: "post",
            dataType: "json",
            complete: function(data) {
                response = data.responseJSON;
                console.log(response);
                if (response.status) {
                    location.href = response.defaultPage;
                } else {
                    message("info","Login fail",0);
                }
            }
        });
        return false;
    })

    $(".logo").addClass("uk-animation-slow")
})