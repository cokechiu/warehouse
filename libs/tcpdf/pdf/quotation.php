<?php 
    include "../../datatables/lib/DataTables.php";
    include "../../../phpclass/util.php";

    // Include the main TCPDF library (search for installation path).
    require_once('tcpdf_include.php');


    $util = new util();
    $util -> setDebug(false);
    
	foreach($_REQUEST as $key => $value) {
		$$key = addslashes($value);
	}

    // Prepare quotation data
    $sql =  "
        select
            q.quotationId,
            q.projectName,
            q.createDate,
            q.expiryDate,
            c.name as customerName,
            c.address as customerAddress,
            cc.name contactName,
            cc.email contactEmail
        from
            quotation q
                left join customer c 
                    on c.customerId = q.customerId
                left join customerContact cc
                    on cc.customerContactId = q.customerContactId
        where
            quotationId = '".$quotationId."'
    ";
    $quotationHead = $db->sql($sql)->fetch();


    // Prepare Items
    $sql = "
        select
            *
        from
            quotationItem i
        where
            i.quotationId = '".$quotationId."'
        order by
            i.optional, i.dorder asc
    ";

    $items = "";

    $grandTotal = 0;
    $subTotal = 0;
    foreach( $db->sql($sql)->fetchAll() as $item ) {
        $subTotal = $item["quantity"] * $item["unitPrice"];
        $grandTotal += $subTotal;
        $items .= "
            <tr>
                <td>".$item['name']."<br>".$item['description']."</td>
                <td align=\"center\">".$item["quantity"]."</td> 
                <td align=\"right\">HKD ".$item["unitPrice"]."</td>
                <td align=\"right\">HKD ".$subTotal."</td> 
            </tr>
            <tr>
                <td colspan=\"4\"><br><hr><br></td>
            </tr>
        ";
    }

    $head = '
        <table style="width:100%; font-size: 10pt;">
            <tr>
                <td style="text-align:center">
                    <img src="/images/logo_quotation1.png" width="160"><br><br>
                </td>
            </tr>
            <tr>
                <td style="width:60%">
                    <span><br></span>
                    <span>Bill to : </span><span><br>'.$quotationHead['customerName'].'<br><br></span>
                    <span>Address : <br>'.$quotationHead['customerAddress'].'<br><br></span>
                    <span>Contact : <br>'.$quotationHead['contactName'].', '.$quotationHead['contactEmail'].'<br><br></span>

                </td>
                <td style="text-align:right">
                    <span><br></span>
                    Quotation number : '.str_replace("-","",$quotationHead['createDate']).$quotationHead['quotationId'].'<br><br>
                    Create date : '.$quotationHead['createDate'].'<br><br>
                    Expiry date : '.$quotationHead['expiryDate'].'<br><br>
                    Grand total (HKD) : '.$grandTotal.'
                </td>
            </tr>
        </table>
    ';


    // Prepare Terms
    $sql = "
        select
            *
        from
            quotation q
                left join paymentTerms t
                    on t.paymentTermsId = q.paymentTermsId
        where
            q.quotationId = '".$quotationId."'
    ";
    $paymentTerms = $db->sql($sql)->fetch();

    /********************************************** Create PDF ***************************************/

    ini_set('memory_limit', '256M');

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetPrintHeader(false);
    $pdf->SetPrintFooter(true);

    $HEADER_TITLE = "Quotation";

    // set default header data
    //$pdf->SetHeaderData($_SERVER["DOCUMENT_ROOT"]."images/logo.png", PDF_HEADER_LOGO_WIDTH, "", "", array(0,0,0), array(0,0,0));

    // set default footer data
    $pdf->setFooterData(array(0,64,0), array(255,255,255));

    // set header and footer fonts
    //$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margin
    $marginTop = 10;
    $pdf->SetMargins(PDF_MARGIN_LEFT, $marginTop, PDF_MARGIN_RIGHT);
    //$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    // ---------------------------------------------------------

    // set default font subsetting mode
    //$pdf->setFontSubsetting(true);

    // Set font
    // dejavusans is a UTF-8 Unicode font, if you only need to
    // print standard ASCII chars, you can use core fonts like
    // helvetica or times to reduce file size.
    //$pdf->SetFont('dejavusans', '', 14, '', true);
    $fontname = TCPDF_FONTS::addTTFfont('../fonts/wqy_microhei.ttf', 'TrueTypeUnicode', '', 32);
    $pdf->SetFont($fontname, '', 10);
    $pdf->setHeaderFont(Array($fontname, '', 10));



    // set text shadow effect
    //$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

    // Add a page
    // This method has several options, check the source code documentation for more information.
    $pdf->AddPage();
    
    // Set some content to print
    //<img src="/images/logo_quotation1.png" width="160"><br><br>
    $html = <<<EOT
        <style>
            td {
                font-size: 10pt;
            }
        </style>
        {$head}
        <br><br><br><br>
        <table style="width:100%">
            <tr>
                <td style="width:60%">Item</td>
                <td align="center" style="width:10%">Quantity</td>
                <td align="right" style="width:15%">Price</td>
                <td align="right" style="width:15%">Amount</td>
            </tr>
            <tr>
                <td colspan="4"><br><hr><br></td>
            </tr>
            {$items}
        </table>
    EOT;
    $pdf->writeHTML($html, true, false, true, false, '');

    $pdf->AddPage();

    $html = <<<EOT
        <style>
            td {
                font-size: 10pt;
            }
        </style>
        <table style="width:100%">
            <tr>
                <td>
                    <span><br></span>
                    <span>Payment terms:<br><br></span>
                    <span>{$paymentTerms["description"]}<br><br></span>
                    
                </td>
            </tr>
        </table>
        <table style="width:100%">
            <tr>
                <td colspan="2"><br><hr><br></td>
            </tr>
            <tr>
                <td style="width:50%">
                    LAGFREESERVER.COM<BR><BR>
                    Signature : <br><br><br><br>
                    Name : CHIU HOI LOK<br><br>
                    Date : {$quotationHead["createDate"]}
                </td>
                <td>
                    {$quotationHead["customerName"]}<BR><BR>
                    Signature : <br><br><br><br>
                    Name : <br><br>
                    Date : 
                </td>
            </tr>
        </table>
    EOT;

    $pdf->writeHTML($html, true, false, true, false, '');
    
    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.
    switch($action){
        case "download":
            $pdf->Output("quotation-".$quotationHead["quotationId"].".pdf", 'D');
            break;
        case "preview":
            $pdf->Output("quotation-".$quotationHead["quotationId"].".pdf", 'I');
            break;
    }
    
    //============================================================+
    // END OF FILE
    //============================================================+

?>