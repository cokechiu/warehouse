<?php

    $datatablesHtml = '
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            <thead>
                <tr>
                   {{fields}} 
                </tr>
            </thead>
        </table>
    ';

    $fieldHtml = '
        <th>{{Name}}</th>
    ';

?>