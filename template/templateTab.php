<?php

    // Tabs template
    $tabHtml = '
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            {{tabItems}}
        </ul>

        <div class="tab-content">
            {{tabPanelItems}}
        </div>
    ';

    $tabItemsHtml = '
        <li class="nav-item">
            <a class="nav-link {{active}}" id="{{name}}-tab" data-toggle="tab" href="#{{name}}" role="tab">
                {{name}}
            </a>
        </li>
    ';

    $tabPanelItemsHtml = '
        <div class="tab-pane {{active}}" id="{{name}}" role="tabpanel" aria-labelledby="{{name}}-tab">
            <div class="row mt-3">
                {{cards}}
            </div>
        </div>
    ';

?>