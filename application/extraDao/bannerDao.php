<?php
	include( "../../config/configWebsite.php" );
	include( adminAbsolutePath."api/isLoginDao.php");
	
	use
		DataTables\Editor,
		DataTables\Editor\Field,
		DataTables\Editor\Format,
		DataTables\Editor\Mjoin,
		DataTables\Editor\Upload,
		DataTables\Editor\Options,
		DataTables\Editor\Validate;
	
	$tableName = "banner";

	// Upload file setting [Banner]
	$thumbnail1 = new stdclass();
	$thumbnail1->k = 1000;
	$thumbnail1->m = $thumbnail1->k * $thumbnail1->k;
	$thumbnail1->table = "bannerImage";
	$thumbnail1->primaryKey = "bannerImageId";
	$thumbnail1->uploadLimit = $thumbnail1->m * 2;
	$thumbnail1->uploadLimitMsg = "Files must be smaller than 2M";
	$thumbnail1->uploadPath = adminAbsolutePath."upload/bannerImage/";
	$thumbnail1->fileType = array( 'png', 'jpg', 'gif' );
	$thumbnail1->fileTypeMsg = "Image only ( png / jpg / gif)";

	Editor::inst( $db, $tableName, $tableName.'Id' )
	->debug(true)
    ->fields(
		Field::inst( $tableName.'.'.$tableName.'Id' ),
		Field::inst( $tableName.'.startDate' ),
		Field::inst( $tableName.'.endDate' ),
		Field::inst( $tableName.'.landing' ),
		Field::inst( $tableName.'.active' ),
		Field::inst( $tableName.'.dorder' )->setFormatter( Format::ifEmpty( -1 ) ),
		Field::inst( $tableName.".".$thumbnail1->primaryKey ) /* thumbnail1 id field name */
			->setFormatter( 'Format::ifEmpty', null )
			->upload( Upload::inst( $thumbnail1->uploadPath.'__ID__.__EXTN__' )
				->db( $thumbnail1->table, $thumbnail1->primaryKey, array(
					'filename'    => Upload::DB_FILE_NAME,
					'filesize'    => Upload::DB_FILE_SIZE,
					'web_path'    => Upload::DB_WEB_PATH
				) )
				->dbClean( function ( $data ) {
					// Remove the files from the file system
					for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
						
						global $systemRootPath;
						
						unlink( $systemRootPath.$data[$i]['web_path'] );

					}
	
					// Have Editor remove the rows from the database
					return true;
				} )
				->validator( Validate::fileSize( $thumbnail1->uploadLimit, $thumbnail1->uploadLimitMsg ) )
                ->validator( Validate::fileExtensions( $thumbnail1->fileType, $thumbnail1->fileTypeMsg ) )
			),
		Field::inst( $thumbnail1->table.'.web_path as image' )
	)
	->leftJoin($thumbnail1->table, $thumbnail1->table.".".$thumbnail1->primaryKey,"=", $tableName.".".$thumbnail1->primaryKey)
	->on( 'preCreate', function ( $editor, $values ) {
		global $tableName;

        // On create update all the other records to make room for our new one
        $editor->db()
            ->query( 'update', $tableName )
            ->set( 'dorder', 'dorder+1', false )
            ->where( 'dorder', $values[$tableName]['dorder'], '>=' )
            ->exec();
    } )
	->on('postRemove',function( $editor, $id, $values ) {
		global $tableName;

        // On remove, the sequence needs to be updated to decrement all rows
        // beyond the deleted row. Get the current reading order by id (don't
        // use the submitted value in case of a multi-row delete).
 
        $editor->db()
            ->query( 'update', $tableName )
            ->set( 'dorder', 'dorder-1', false )
            ->where( 'dorder', $values[$tableName]['dorder'], '>' )
            ->exec();
	})
    ->process( $_POST )
    ->json();
?>